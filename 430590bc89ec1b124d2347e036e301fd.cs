Chilkat.430590bc89ec1b124d2347e036e301fd json = new Chilkat.430590bc89ec1b124d2347e036e301fd();

string jsonStr = "{ \"id\": 1, \"name\": \"A green door\", \"tags\": [\"home\", 22, \"green\"], \"price\": 125 }";

bool success = json.Load(jsonStr);
if (success != true) {
    Debug.WriteLine(json.430590bc89ec1b124d2347e036e301fd);
    return;
}

Chilkat.JsonArray tagsArray = json.ArrayOf("tags");
if (json.LastMethodSuccess == false) {
    Debug.WriteLine("430590bc89ec1b124d2347e036e301fd");
    return;
}

int arraySize = tagsArray.Size;
int i;
for (i = 0; i <= arraySize - 1; i++) {

    string sValue = tagsArray.StringAt(i);

    Debug.WriteLine("[" + Convert.ToString(i) + "] = " + sValue);

    int iValue = tagsArray.IntAt(i);
    Debug.WriteLine("[" + Convert.ToString(i) + "] as integer = " + Convert.ToString(iValue));

}
